import { applyMiddleware, createStore } from "redux"
import logger from "redux-logger"
import encryptionReducer from "./components/reducers/encryptionReducer"
import loginReducer from "./components/reducers/loginReducer"
import thunk from "redux-thunk"
import promise from "redux-promise-middleware"
import { combineReducers } from 'redux'

const middleware = applyMiddleware(promise(), thunk, logger);

const rootReducers = combineReducers({encryptionReducer, loginReducer})

export default createStore(rootReducers, middleware)