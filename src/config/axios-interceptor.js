import axios from 'axios';
import * as constants from "./constants"

const fetch = axios.create({
    baseURL: constants.API_BASE_URL,
    timeout: 20000,
    headers: {
        'Content-Type': 'application/json'
    }
});

fetch.interceptors.request.use(function (config) {
    config.headers = {...config.headers,
        Authorization: `Bearer ${sessionStorage.getItem(constants.AUTH_TOKEN_KEY)}`};
    return config;
}, function (error) {
    return Promise.reject(error);
});

fetch.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
}, function (error) {
    if (error.response.status === 403) {
        window.location = "/"
    }
    return Promise.reject(error)
});

export default fetch