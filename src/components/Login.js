import React, {Component} from "react";
import {connect} from "react-redux";
import {login, logout} from "./reducers/loginReducer"
import {Redirect} from "react-router-dom";


export class Login extends Component {

    constructor(props) {
        super(props)
        this.submit = this.submit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this.props.logout()
    }

    submit(event) {
        event.preventDefault()
        this.props.login(this.state)
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({ [name]: value });
    };

    render() {
        if (this.props.isAuthenticated) {
            return <Redirect to='/encryption' />;
        }
        return (
            <div className="jumbotron d-flex align-items-center">
                <div className="container">
                    <form className="align-items-center" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Username</label>
                            <input type="text" className="form-control" name="username"
                                   placeholder="Enter Username" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" name="password"
                                   placeholder="Password" onChange={this.handleChange}/>
                        </div>
                        <button className="btn btn-primary" onClick={this.submit}>Submit</button>
                    </form>
                    <div className="row mt-1">
                        {this.props.error ?
                            <div className="alert alert-danger col-md-12 mt-1" role="alert">
                                {this.props.errorMessage}
                            </div> : <div></div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        error: state.loginReducer.error,
        errorMessage: state.loginReducer.errorMessage,
        isAuthenticated: state.loginReducer.authenticated,
    }
};

const mapDispatchToProps = {login, logout}

export default connect(mapStateToProps, mapDispatchToProps)(Login)