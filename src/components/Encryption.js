import React, {Component} from "react";
import {connect} from "react-redux";
import {getEncryptedMessages, encryptMessage, decryptMessage, getEncryptedMessage} from "./reducers/encryptionReducer"
import {EncryptionTableLine} from "./EncryptionTableLine";

export class Encryption extends Component {

    constructor(props) {
        super(props)
        this.update = this.update.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.decrypt = this.decrypt.bind(this)
        this.getEncryptedMessage = this.getEncryptedMessage.bind(this)
    }

    componentWillMount() {
        this.props.getEncryptedMessages()
    }

    handleChange(e) {
        this.setState({input: e.target.value});
    }

    update() {
        this.props.encryptMessage(this.state.input)
    }

    decrypt(id) {
        this.props.decryptMessage(id)
    }

    getEncryptedMessage(id) {
        this.props.getEncryptedMessage(id)
    }

    render() {
        const data = this.props;
        return (
            <div className="container h-100">
                {
                    data.messages.map((item, index) => (
                        <EncryptionTableLine key={item.id} item={item} index={index} decrypt={this.decrypt}
                                             encrypt={this.getEncryptedMessage}/>
                    ))
                }
                <div className="row">
                    <div className="align-middle text-center col-md-11">
                        <input className="form-control" placeholder="Message" onChange={this.handleChange}/>
                    </div>
                    <div className="align-middle text-center col-md-1">
                        <button className="btn btn-primary" onClick={this.update}>Encrypt</button>
                    </div>
                </div>
                <div className="row">
                    {data.error ?
                        <div className="alert alert-danger col-md-12 mt-1" role="alert">
                            {data.errorMessage}
                        </div> : <div></div>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.encryptionReducer.messages,
        error: state.encryptionReducer.error,
        errorMessage: state.encryptionReducer.errorMessage
    }
};

const mapDispatchToProps = {getEncryptedMessage, encryptMessage, decryptMessage, getEncryptedMessages}

export default connect(mapStateToProps, mapDispatchToProps)(Encryption)