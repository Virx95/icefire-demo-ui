import fetch from "../../config/axios-interceptor"
import { REQUEST, SUCCESS, FAILURE } from "./util/action-type-util"

export const ACTION_TYPES = {
    GET_ALL_ENCRYPED_MESSAGES: 'GET_ALL_ENCRYPED_MESSAGES',
    ENCRYPT_MESSAGE: 'ENCRYPT_MESSAGE',
    DECRYPT_MESSAGE: 'DECRYPT_MESSAGE',
    GET_ENCRYPTED_MESSAGE: 'GET_ENCRYPTED_MESSAGE'
};

const initialState = {
    error: false,
    loading: false,
    errorMessage: null,
    messages: []
};

//reducer
export default (state = initialState, action) => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.GET_ALL_ENCRYPED_MESSAGES):
            return {
                ...state,
                loading: true,
                error: false,
            };
        case SUCCESS(ACTION_TYPES.GET_ALL_ENCRYPED_MESSAGES):
            return {
                ...state,
                loading: false,
                error: false,
                messages: action.payload.data.map(item => {return {...item, encrypted: true}}),
            };
        case FAILURE(ACTION_TYPES.GET_ALL_ENCRYPED_MESSAGES):
            return {
                ...state,
                loading: false,
                errorMessage: action.payload.response.data.message,
                error: true,
            };
        case REQUEST(ACTION_TYPES.ENCRYPT_MESSAGE):
            return {
                ...state,
                loading: true,
                error: false,
            };
        case SUCCESS(ACTION_TYPES.ENCRYPT_MESSAGE):
            action.payload.data.encrypted = true;
            return {
                ...state,
                loading: false,
                error: false,
                messages: [...state.messages, action.payload.data],
            };
        case FAILURE(ACTION_TYPES.ENCRYPT_MESSAGE):
            return {
                ...state,
                loading: false,
                error: true,
                errorMessage: action.payload.response.data.message
            };
        case REQUEST(ACTION_TYPES.DECRYPT_MESSAGE):
            return {
                ...state,
                loading: true,
                error: false,
            };
        case SUCCESS(ACTION_TYPES.DECRYPT_MESSAGE):
            return {
                ...state,
                loading: false,
                error: false,
                messages: updateObjectInArray(state.messages, action.payload.data, false)
            };
        case FAILURE(ACTION_TYPES.DECRYPT_MESSAGE):
            return {
                ...state,
                loading: false,
                error: true,
                errorMessage: action.payload.response.data.message
            };
        case REQUEST(ACTION_TYPES.GET_ENCRYPTED_MESSAGE):
            return {
                ...state,
                loading: true,
                error: false,
            };
        case SUCCESS(ACTION_TYPES.GET_ENCRYPTED_MESSAGE):
            console.log("returned value", action)
            return {
                ...state,
                loading: false,
                error: false,
                messages: updateObjectInArray(state.messages, action.payload.data, true)
            };
        case FAILURE(ACTION_TYPES.GET_ENCRYPTED_MESSAGE):
            return {
                ...state,
                loading: false,
                error: true,
                errorMessage: action.payload.response.data.message
            };
        default:
            return state
    }
};

function updateObjectInArray(array, action, encrypted) {
    return array.map(item => {
        if (item.id !== action.id) {
            return item
        }
        return {
            ...item,
            ...action,
            encrypted: encrypted
        }
    })
}

//actions
export const encryptMessage = (message) => ({
    type: ACTION_TYPES.ENCRYPT_MESSAGE,
    payload: fetch.put("/encrypt", {message: message})
});

export const getEncryptedMessages = () => ({
    type: ACTION_TYPES.GET_ALL_ENCRYPED_MESSAGES,
    payload: fetch.get("/encrypt")
});

export const decryptMessage = id => ({
    type: ACTION_TYPES.DECRYPT_MESSAGE,
    payload: fetch.get("/decrypt/" + id)
});

export const getEncryptedMessage = id => ({
    type: ACTION_TYPES.GET_ENCRYPTED_MESSAGE,
    payload: fetch.get("/encrypt/" + id)
});