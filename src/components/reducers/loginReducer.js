import {FAILURE, REQUEST, SUCCESS} from "./util/action-type-util";
import fetch from "../../config/axios-interceptor"
import * as constants from "../../config/constants"

export const ACTION_TYPES = {
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT',
};

const initialState = {
    error: false,
    errorMessage: null,
    authenticated: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.LOGIN):
            return {
                ...state,
                error: false,
            };
        case SUCCESS(ACTION_TYPES.LOGIN): {
            saveTokenToSessionStorage(action)
            return {
                ...state,
                error: false,
                authenticated: true
            };
        }
        case FAILURE(ACTION_TYPES.LOGIN): {
            return {
                ...state,
                error: true,
                errorMessage: action.payload.response.data.message
            };
        }
        case ACTION_TYPES.LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }
}

function saveTokenToSessionStorage(action) {
    const jwt = action.payload.data.token;
    sessionStorage.setItem(constants.AUTH_TOKEN_KEY, jwt);
}

//actions
export const login = (credentials) => ({
    type: ACTION_TYPES.LOGIN,
    payload: fetch.post("/user/login", credentials)
});

export const logout = () => {
    sessionStorage.clear();
    return {
        type: ACTION_TYPES.LOGOUT
    }
};