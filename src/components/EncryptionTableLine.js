import React, {Component} from "react";


export class EncryptionTableLine extends Component {

    constructor(props) {
        super(props)
        this.decrypt = this.decrypt.bind(this)
        this.encrypt = this.encrypt.bind(this)
    }

    decrypt() {
        this.props.decrypt(this.props.item.id)
    }

    encrypt() {
        this.props.encrypt(this.props.item.id)
    }

    render() {
        return (
            <div key={this.props.item.id} className="row mb-1 align-items-center border">
                <div className="align-middle text-center col-md-1">{this.props.index}</div>
                <div className="align-middle col-10 word-break">{this.props.item.message}</div>
                <div className="align-middle text-center col-1">
                    {this.props.item.encrypted ?
                        <button type="submit" className="btn btn-primary" onClick={this.decrypt}>Decrypt</button>
                        :
                        <button type="submit" className="btn btn-primary" onClick={this.encrypt}>Encrypt</button>
                    }

                </div>
            </div>
        )
    }
}