import React from 'react';
import './App.css';
import Encryption from "./components/Encryption"
import {BrowserRouter as Router, Route} from "react-router-dom";
import Login from "./components/Login";


function App() {
  return (
      <Router>
        <Route exact path="/" component={Login}/>
        <Route path="/encryption" component={Encryption}/>
      </Router>
  );
}

export default App;
